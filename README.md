# Entity Component System Experiment #

This is an experiment I'm working on to create an [Entity Component System](http://en.wikipedia.org/wiki/Entity_component_system) in Typescript using Node.js on the backend, require.js and angular on the frontend, with socket.io bridging the gap.

My main goal with this is to use the same code for processing entities on the frontend and the backend with the only differences being how they are compiled.