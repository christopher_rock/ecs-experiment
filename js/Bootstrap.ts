/// <reference path="../typings/requirejs/require.d.ts" />

/// <reference path="RequireConfig.ts" />
/// <reference path="AppMain.ts" />

//import main = require('AppMain');

require(['domReady!', 'angular', 'AppMain'], (document, ng, main) => {
    ng.bootstrap(document, ['app']);
});
