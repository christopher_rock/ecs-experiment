/// <reference path="../typings/socket.io/socket.io.d.ts" />
/// <reference path="../typings/node/node.d.ts" />

import PositionComponent = require('app/Components/PositionComponent');
import CN = require('app/Components/ComponentName');
import Entity = require('app/Entity');
import System = require('app/System');
import PositionSystem = require('app/Systems/PositionSystem');
import GS = require('app/GameSettings');

class GameMain {
    private entities:Entity[];
    private systems:System[];

    private tickTimer:number;
    private tickCount:number;
    private intervalID:any;

    constructor() {
        this.entities = new Array<Entity>();
        this.systems = new Array<System>();

        var testEnt:Entity = new Entity("123");
        var comp:PositionComponent = new PositionComponent(1, 2);
        comp.velocity.x = .5;
        comp.velocity.y = 1.0;
        testEnt.addComponent(comp);

        this.entities.push(testEnt);

        this.tickTimer = 0;
        this.tickCount = 0;

        this.systems.push(new PositionSystem());

        this.intervalID = setInterval(() => {
            this.Update();
        }, GS.TICK_TIME * 1000);
    }

    Update() {
        this.tickCount += 1;
        this.tickTimer += GS.TICK_TIME;

        this.systems.forEach(function(system:System) {
            system.Update(this.data, GS.TICK_TIME);
        }, this);
    }
}
