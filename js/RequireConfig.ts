require.config({
    baseUrl : '/js/',
    paths : {
        'jquery': 'lib/jquery/dist/jquery',
        'angular': 'lib/angular/angular',
        'domReady': 'lib/requirejs-domready/domReady',
        'socket.io-client': 'lib/socket.io-client/socket.io'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'angular': {
            exports: 'angular'
        },
        'socket.io-client': {
            exports: 'io'
        }
    }
});

declare module 'angular' {
    var angular: ng.IAngularStatic;
    export = angular;
}
