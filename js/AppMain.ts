/// <reference path="../typings/angularjs/angular.d.ts" />
/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/socket.io-client/socket.io-client.d.ts" />

declare var require:(moduleId:string) => any;

import ng = require("angular");
import io = require("socket.io-client");
import test = require("app/Test");
import entityInfoDirective = require("app/EntityDirective");

var app = ng.module('app', []);
app.controller('TestController', test);
app.directive('entityInfo', entityInfoDirective);

var socket:io.Socket = io.connect();

export = app;
