
import ng = require("angular");
import PositionComponent = require('app/Components/PositionComponent');
import CN = require('app/Components/ComponentName');
import Entity = require('app/Entity');
import System = require('app/System');
import PositionSystem = require('app/Systems/PositionSystem');
import GS = require('app/GameSettings');

class TestController {

    private data: Entity[];

    private tickTimer:number;
    private tickCount:number;
    private intervalID:any;
    private systems:System[];

    public static $inject = [
        '$scope',
        '$interval'
    ];

    constructor(
        private $scope,
        $interval
    ) {
        this.data = new Array<Entity>();
        this.systems = new Array<System>();

        var testEnt:Entity = new Entity("123");
        var comp:PositionComponent = new PositionComponent(1, 2);
        comp.velocity.x = .5;
        comp.velocity.y = 1.0;
        testEnt.addComponent(comp);

        this.data.push(testEnt);

        $scope.data = this.data;

        this.tickTimer = 0;
        this.tickCount = 0;

        this.intervalID = $interval(() => {
            this.Update();
        }, GS.TICK_TIME * 1000);

        this.systems.push(new PositionSystem());
    }

    Update() {
        this.tickCount += 1;
        this.tickTimer += GS.TICK_TIME;

        this.systems.forEach(function(system:System) {
            system.Update(this.data, GS.TICK_TIME);
        }, this);
    }
}

export = TestController;
