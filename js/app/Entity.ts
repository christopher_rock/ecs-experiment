
import Component = require('app/Component');
import ComponentName = require('app/Components/ComponentName');

class Entity {

    private _id: string;
    private _components: any;

    constructor(id:string) {
        this._id = id;
        this._components = {};
    }

    get id(): string {
        return this._id;
    }

    addComponent(newComponent:Component): void {
        this._components[newComponent.name] = newComponent;
    }

    removeComponent(componentName:ComponentName): Component {
        var toRet = this.getComponent(componentName);
        if (toRet)
            delete this._components[componentName];
        return toRet;
    }

    hasComponent(componentName:ComponentName): boolean {
        return this._components.hasOwnProperty(componentName);
    }

    getComponent(componentName:ComponentName): Component {
        return this._components[componentName];
    }
}

export = Entity;
