
import Entity = require('app/Entity');

interface System {
    Update(entities:Entity[], timeDelta:number): void;
}

export = System;
