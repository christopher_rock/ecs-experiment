
import ng = require("angular");
import PositionComponent = require('app/Components/PositionComponent');
import CN = require('app/Components/ComponentName');
import Entity = require('app/Entity');
import PositionSystem = require('app/Systems/PositionSystem');

var template:String = "<span>{{name}} - x: {{x|number}} y: {{y|number}} vel: {{velX|number}} {{velY|number}}</span>";

function entityDirective() {
    return {
        restrict: 'E',
        scope: {
            entity: "="
        },
        controller: function($scope, $element) {
            if ($scope.entity) {
                $scope.$watch('entity', function(newValue, oldValue) {
                    $scope.name = $scope.entity.id;
                    if ($scope.entity.hasComponent(CN.POSITION)) {
                        var c:PositionComponent = $scope.entity.getComponent(CN.POSITION);

                        $scope.x = c.x;
                        $scope.y = c.y;
                        $scope.velX = c.velocity.x;
                        $scope.velY = c.velocity.y;
                    }
                }, true);
            }
        },
        template: template
    }
}

export = entityDirective;
