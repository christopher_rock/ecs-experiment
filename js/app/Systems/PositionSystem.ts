
import System = require('app/System');
import Entity = require('app/Entity');
import PositionComponent = require('app/Components/PositionComponent');
import CN = require('app/Components/ComponentName');

class PositionSystem implements System {
    Update(entities:Entity[], timeDelta:number) {
        entities.forEach(function (item:Entity) {
            if (!item.hasComponent(CN.POSITION))
                return;

            var c:PositionComponent = <PositionComponent>item.getComponent(CN.POSITION);

            c.x += c.velocity.x * timeDelta;
            c.y += c.velocity.y * timeDelta;
        });
    }
}

export = PositionSystem;
