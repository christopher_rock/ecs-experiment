
import Component = require('app/Component');
import ComponentName = require('app/Components/ComponentName');
import Vectors = require('app/Utility/Vectors');
import Vector2 = Vectors.Vector2;

class PositionComponent extends Component {
    private _x:number;
    private _y:number;
    private _velocity:Vector2;
    constructor(x:number, y:number) {
        this.name = ComponentName.POSITION;
        this._x = x;
        this._y = y;
        this._velocity = new Vector2();
        super();
    }

    get x(): number { return this._x; }
    set x(value:number) { this._x = value; }
    get y(): number { return this._y; }
    set y(value:number) { this._y = value; }
    get velocity(): Vector2 { return this._velocity; }
}

export = PositionComponent;
