module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    typescript: {
      client: {
        src: ['js/Bootstrap.ts'],
        //dest: 'build/application.js',
        options: {
          module: 'amd', //or commonjs
          target: 'es5', //or es3
          declaration: false
        }
      },
      clientWatch: {
        src: ['js/Bootstrap.ts'],
        //dest: 'build/application.js',
        options: {
          module: 'amd', //or commonjs
          target: 'es5', //or es3
          declaration: false,
          watch: true
        }
      },
      server: {
        src: ['js/ServerBootstrap.ts'],
        dest: 'serverjs',
        options: {
          module: 'commonjs', //or commonjs
          target: 'es5', //or es3
          declaration: false
        }
      }
    },
    // uglify: {
    //   options: {
    //     banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
    //   },
    //   build: {
    //     src: 'build/application.js',
    //     dest: 'build/application.min.js'
    //   }
    // }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-typescript');

  // Default task(s).
  grunt.registerTask('default', ['typescript:clientWatch']);
  grunt.registerTask('server', ['typescript:server']);

};
