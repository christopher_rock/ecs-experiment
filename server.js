var express = require('express');
var staticServer = require('serve-static');
var http = require('http');

var app = express();
app.use(staticServer(__dirname));

server = http.Server(app);

var io = require('socket.io')(server);

io.on('connection', function(socket){
  console.log('a user connected');
});

server.listen(3000, function() {
    console.log("Listening for connections on port 3000");
});
